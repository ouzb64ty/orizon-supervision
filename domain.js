/* Requires */

const express = require('express');
const jwt = require('jsonwebtoken');
const app = express();
const cors = require('cors');
const request = require('request');
const fs = require('fs');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');

/* Config */
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static('public'));
app.use(express.static('node_modules/js-datepicker/dist'));
app.use(express.static('node_modules/mdbootstrap/js'));
app.set('view engine', 'ejs');
app.set('views', __dirname + '/views');
app.use(cors());


/* Home */

app.get('/', (req, res) => {
    console.log('[ROUTER] ' + req.url);

    var token = req.cookies.token;

    jwt.verify(token, 'jwtRS256.key', (err, user) => {
        if (err) {
            res.redirect('/login');
        } else {
            res.render('index');
        }
    });
});

/* Welcome (first connection) */

app.get('/welcome', (req, res) => {
    console.log('[ROUTER] ' + req.url);

    var token = req.cookies.token;

    jwt.verify(token, 'jwtRS256.key', (err, user) => {
        if (err) {
            res.redirect('/login');
        } else {
            res.render('welcome');
        }
    });
});

/* Cameras Monitor */

app.get('/monitor', (req, res) => {
    console.log('[ROUTER] ' + req.url);

    var token = req.cookies.token;

    jwt.verify(token, 'jwtRS256.key', (err, user) => {
        if (err) {
            res.redirect('/login');
        } else {
            request.get({

                url: 'http://127.0.0.1:8001/orizon/v1/cameras/namespaces',
                auth: { 'bearer': token }

            }, function(err, httpResponse, body) {

                var namespaces = JSON.parse(body);

                res.render('monitor', namespaces);

            });
        }
    });
});

app.get('/screen-monitor/:namespace', (req, res) => {
    console.log('[ROUTER] ' + req.url);

    var token = req.cookies.token;

    jwt.verify(token, 'jwtRS256.key', (err, user) => {
        if (err) {
            res.redirect('/login');
        } else {
            res.render('screenMonitor');
        }
    });
});

/* Add Camera */

app.get('/add_camera', (req, res) => {
    console.log('[ROUTER] ' + req.url);

    var token = req.cookies.token;

    jwt.verify(token, 'jwtRS256.key', (err, user) => {
        if (err) {
            res.redirect('/login');
        } else {
            res.render('add_camera');
        }
    });
});

/* Alarms Camera */

app.get('/alarms_camera', (req, res) => {
    console.log('[ROUTER] ' + req.url);

    var token = req.cookies.token;

    jwt.verify(token, 'jwtRS256.key', (err, user) => {
        if (err) {
            res.redirect('/login');
        } else {
            request.get({

                url: 'http://127.0.0.1:8003/vision/v1/alarms',
                auth: { 'bearer': token }

            }, function(err, httpResponse, body) {

                const bodyJSON = JSON.parse(body);

                res.render('alarms_camera', {'body': bodyJSON});

            });
        }
    });
});

/* List Cameras */

app.get('/list_camera/:page?', (req, res) => {
    console.log('[ROUTER] ' + req.url);

    var token = req.cookies.token;
    var page = req.params.page;
    const ObjectID = require('mongodb').ObjectID;

    if (page == undefined || page < 0) page = 0;

    jwt.verify(token, 'jwtRS256.key', (err, user) => {

        if (err) {

            res.redirect('/login');

        } else {

            request.get({

                url: 'http://127.0.0.1:8001/orizon/v1/cameras/page/' + page,
                auth: { 'bearer': token }

            }, function(err, httpResponse, body) {

                const bodyJSON = JSON.parse(body);

                bodyJSON.page = page;
                res.render('list_camera', {'body': bodyJSON});

            });

        }

    });
});

/* Profil */

app.get('/profil/:username', (req, res) => {
    console.log('[ROUTER] ' + req.url);

    var token = req.cookies.token;
    let bodyJSON = {};

    jwt.verify(token, 'jwtRS256.key', (err, user) => {
        if (err) {
            res.redirect('/login');
        } else {
            if (user.payload.is_admin == true) {

                /* For admin */
                request.get({
                    url: 'http://127.0.0.1:8001/orizon/v1/auth/connections',
                    auth: { 'bearer': token }
                }, function(err, httpResponse, body) {

                    /* Get all connections */
                    const connections = JSON.parse(body);

                    request.get({
                        url: 'http://127.0.0.1:8001/orizon/v1/auth/users/' + req.params.username,
                        auth: { 'bearer': token }
                    }, function(err, httpResponse, body) {

                        /* Get user datas */
                        const user = JSON.parse(body);

                        if (user == null) {
                            res.render('404_page');
                        } else {
                            request.get({
                                url: 'http://127.0.0.1:8001/orizon/v1/cameras/username/' + req.params.username,
                                auth: { 'bearer': token }
                            }, function(err, httpResponse, body) {

                                /* Get cameras by user */
                                const cameras = JSON.parse(body);

                                bodyJSON = connections;
                                bodyJSON.user = user;
                                bodyJSON.cameras = cameras.cameras;
                                res.render('profil', {'body': bodyJSON});

                            });
                        }
                    });

                });

            } else {

                /* For user */
                res.redirect('/');

            }
        }
    });
});

/* My account */

app.get('/my_account/:notification?', (req, res) => {
    console.log('[ROUTER] ' + req.url);

    var token = req.cookies.token;
    let bodyJSON = {};

    jwt.verify(token, 'jwtRS256.key', (err, user) => {
        if (err) {
            res.redirect('/login');
        } else {
            if (user.payload.is_admin == true) {
                /* For admin */
                request.get({
                    url: 'http://127.0.0.1:8001/orizon/v1/auth/connections',
                    auth: { 'bearer': token }
                }, function(err, httpResponse, body) {
                    const connections = JSON.parse(body);

                    request.get({
                        url: 'http://127.0.0.1:8001/orizon/v1/auth/users',
                        auth: { 'bearer': token }
                    }, function(err, httpResponse, body) {
                        const users = JSON.parse(body);

                        request.get({
                            url: 'http://127.0.0.1:8003/vision/v1/active_cameras',
                            auth: { 'bearer': token }
                        }, function(err, httpResponse, body) {
                            if (err) {
                                bodyJSON.analyses = {};
                            } else {
                                bodyJSON.analyses = JSON.parse(body).active_cameras;
                            }

                            bodyJSON.connections = connections.connections;
                            bodyJSON.users = users.users;
                            bodyJSON.user = user;
                            bodyJSON.notification = req.params.notification;
                            res.render('my_account', {'body': bodyJSON});
                        });
                    });
                });
            } else {
                /* For user */
                request.get({
                    url: 'http://127.0.0.1:8001/orizon/v1/auth/myConnections',
                    auth: { 'bearer': token }
                }, function(err, httpResponse, body) {
                    const connections = JSON.parse(body);

                    bodyJSON = connections;
                    bodyJSON.user = user;
                    res.render('my_account', {'body': bodyJSON});
                });
            }
        }
    });
});
app.post('/my_account/create_user', (req, res) => {
    console.log('[ROUTER] ' + req.url);

    var is_admin_check = false;

    if (req.body.is_admin == "true") is_admin_check = true;
    request.post({
        url:'http://127.0.0.1:8001/orizon/v1/auth/register',
        form: {
            username: req.body.username,
            password: req.body.password,
            conf_password: req.body.conf_password,
            mail: req.body.mail,
            is_admin: is_admin_check /* (for debug) */
        }
    }, function(err, httpResponse, body){
        if (err) throw err;

        if (httpResponse.statusCode == 200)
            res.redirect('/my_account/success');
        else if (httpResponse.statusCode == 400 || httpResponse.statusCode == 404)
            res.redirect('/my_account/failure');
        else
            res.redirect('/my_account');
    })
});

/* Search Cameras */

app.get('/search_camera', (req, res) => {
    console.log('[ROUTER] ' + req.url);

    var token = req.cookies.token;

    jwt.verify(token, 'jwtRS256.key', (err, user) => {
        if (err) {
            res.redirect('/login');
        } else {
            request.get({

                url: 'http://127.0.0.1:8001/orizon/v1/cameras/namespaces',
                auth: { 'bearer': token }

            }, function(err, httpResponse, body) {

                const namespaces_ = JSON.parse(body);

                request.get({

                    url: 'http://127.0.0.1:8001/orizon/v1/cameras',
                    auth: { 'bearer': token }

                }, function(err, httpResponse, body) {

                    const cameras_ = JSON.parse(body);
                    request.get({

                        url: 'http://127.0.0.1:8003/vision/v1/active_cameras',
                        auth: { 'bearer': token }

                    }, function(err, httpResponse, body) {

                        const active_cameras_ = JSON.parse(body);
                        let bodyJSON = {};

                        bodyJSON.namespaces = namespaces_;
                        bodyJSON.cameras = cameras_;
                        bodyJSON.active_cameras = active_cameras_;
                        res.render('search_camera', {'body' : bodyJSON});})

                });

            });
        }
    });
});

/* Analyze Camera */

app.get('/analyse_camera/:id', (req, res) => {
    console.log('[ROUTER] ' + req.url);

    var token = req.cookies.token;

    jwt.verify(token, 'jwtRS256.key', (err, user) => {

        if (err) {

            res.redirect('/login');

        } else {

            request.get({

                url: 'http://127.0.0.1:8002/vision/v1/cameras/' + req.params.id,
                auth: { 'bearer': token }

            }, function(err, httpResponse, body){

                if (err) {

                    throw err;
                    res.redirect('/login');

                } else {

                    if (httpResponse.statusCode == 500)
                        res.redirect('/');
                    else {
                        let camera_ = JSON.parse(body);

                        request.get({

                            url: 'http://127.0.0.1:8002/vision/v1/areas/' + req.params.id,
                            auth: { 'bearer': token }

                        }, function(err, httpResponse, body){

                            let bodyJSON = {};
                            let areas_ = JSON.parse(body);

                            bodyJSON.camera = camera_;
                            bodyJSON.areas = areas_;
                            res.render('analyse_camera', {'body': bodyJSON});
                        });
                    }

                }

            });

        }

    });
});

/* Areas Camera */

app.get('/area_camera/:id', (req, res) => {
    console.log('[ROUTER] ' + req.url);

    var token = req.cookies.token;

    jwt.verify(token, 'jwtRS256.key', (err, user) => {

        if (err) {

            res.redirect('/login');

        } else {

            request.get({

                url: 'http://127.0.0.1:8001/orizon/v1/cameras/id/' + req.params.id,
                auth: { 'bearer': token }

            }, function(err, httpResponse, body){

                if (err) {

                    res.redirect('/login');

                } else {

                    if (httpResponse.statusCode == 500)
                        res.redirect('/');
                    else {
                        let camera_ = JSON.parse(body);
                        let bodyJSON = {};

                        bodyJSON.camera = camera_;
                        res.render('area_camera', {'body': bodyJSON});
                    }

                }

            });

        }

    });
});

/* Add Camera(s) */

app.get('/add_cam', (req, res) => {
    console.log('[ROUTER] ' + req.url);

    var token = req.cookies.token;

    jwt.verify(token, 'jwtRS256.key', (err, user) => {
        if (err) {
            res.redirect('/login');
        } else {
            res.render('add_cam');
        }
    });
});

/* Dalle Camera */

function strncmp(a, b, n){
    return a.substring(0, n) == b.substring(0, n);
}

function filterSearch(checks) {
    var classes = [];
    var colors = [];
    var areas = [];

    if (checks.personne == 'on')
        classes.push(0);
    if (checks.velo == 'on')
        classes.push(1);
    if (checks.voiture == 'on')
        classes.push(2);
    if (checks.moto == 'on')
        classes.push(3);
    if (checks.bus == 'on')
        classes.push(4);
    if (checks.camion == 'on')
        classes.push(5);
    if (checks.sacados == 'on')
        classes.push(6);
    if (checks.sacamain == 'on')
        classes.push(7);
    if (checks.valise == 'on')
        classes.push(8);
    if (checks.animal == 'on')
        classes.push(9);

    if (checks.blanc == 'on')
        colors.push(0);
    if (checks.noir == 'on')
        colors.push(1);
    if (checks.rouge == 'on')
        colors.push(2);
    if (checks.orange == 'on')
        colors.push(3);
    if (checks.jaune == 'on')
        colors.push(4);
    if (checks.vert == 'on')
        colors.push(5);
    if (checks.bleu == 'on')
        colors.push(6);
    if (checks.violet == 'on')
        colors.push(7);
    if (checks.magenta == 'on')
        colors.push(8);

    for (var key in checks) {
        if (strncmp(key, 'area_', 5))
            areas.push(key.substr(5, key.length));
    }

    return ({
        "classes": classes,
        "colors": colors,
        "areas": areas
    });
}

app.get('/dalle_camera/:id', (req, res) => {
    console.log('[ROUTER] ' + req.url);

    var token = req.cookies.token;
    var moment = require('moment');
    var checks;
    var fromTimeSplit = req.query.fromTime.split(":");
    var toTimeSplit = req.query.toTime.split(":");
    var timestampFromDate = Date.parse(req.query.fromDate) / 1000;
    var timestampToDate = Date.parse(req.query.toDate) / 1000;

    if (req.query.n == undefined || req.query.n < 0) req.query.n = 0;

    checks = filterSearch(req.query);
    checks.from = timestampFromDate + (parseInt(fromTimeSplit[0]) * (3600000 / 1000) + (parseInt(fromTimeSplit[1]) * (60000 / 1000)));
    checks.to = timestampToDate + (parseInt(toTimeSplit[0]) * (3600000 / 1000) + (parseInt(toTimeSplit[1]) * (60000 / 1000)));
    checks.from *= 1000;
    checks.to *= 1000;
    jwt.verify(token, 'jwtRS256.key', (err, user) => {
        if (err) {

            res.redirect('/login');

        } else {

            request.get({

                url: 'http://127.0.0.1:8003/vision/v1/detections/cameras/' + req.params.id + '/search/' + req.query.n,
                qs: checks,
                auth: { 'bearer': token }

            }, function(err, httpResponse, body){

                let detections_ = JSON.parse(body);

                request.get({

                    url: 'http://127.0.0.1:8002/vision/v1/areas/' + req.params.id,
                    auth: { 'bearer': token }

                }, function(err, httpResponse, body){

                    let areas_ = JSON.parse(body);
                    let bodyJSON = {};
                    const url = require('url');
                    let nextUrl = new URLSearchParams(req.url);
                    let previousUrl = new URLSearchParams(req.url);
                    let firstUrl = new URLSearchParams(req.url);

                    nextUrl.set('n', String(parseInt(nextUrl.get('n')) + 1));
                    previousUrl.set('n', String(parseInt(previousUrl.get('n')) - 1));
                    firstUrl.set('n', '0');
                    bodyJSON.detections = detections_;
                    bodyJSON.areas = areas_;
                    bodyJSON._id = req.params.id;
                    bodyJSON.page = req.query.n;
                    bodyJSON.nextUrl = 'http://127.0.0.1:8000' + nextUrl.toString().replace('%2F', '/').replace('%2F', '/').replace('%3F', '?');
                    bodyJSON.previousUrl = 'http://127.0.0.1:8000' + previousUrl.toString().replace('%2F', '/').replace('%2F', '/').replace('%3F', '?');
                    bodyJSON.firstUrl = 'http://127.0.0.1:8000' + firstUrl.toString().replace('%2F', '/').replace('%2F', '/').replace('%3F', '?');
                    res.render('dalle_camera', {'body': bodyJSON, 'moment': moment});

                });

            });

        }
    });
});

/* Disconnection */

app.get('/disconnection', (req, res) => {
    console.log('[ROUTER] ' + req.url);

    var token = req.cookies.token;

    jwt.verify(token, 'jwtRS256.key', (err, user) => {
        if (!err) res.clearCookie('token');
        res.redirect('/');
    });
});

/* Login with POST action from form */

app.get('/login', (req, res) => {
    console.log('[ROUTER] ' + req.url);

    var token = req.cookies.token;

    jwt.verify(token, 'jwtRS256.key', (err, user) => {
        if (err) {
            res.render('login');
        } else {
            res.redirect('/');
        }
    });
});
app.get('/login/bad', (req, res) => {
    console.log('[ROUTER] ' + req.url);

    var token = req.cookies.token;

    jwt.verify(token, 'jwtRS256.key', (err, user) => {
        if (err) {
            res.render('badlogin');
        } else {
            res.redirect('/');
        }
    });
});
app.get('/login/reconnection', (req, res) => {
    console.log('[ROUTER] ' + req.url);

    var token = req.cookies.token;

    jwt.verify(token, 'jwtRS256.key', (err, user) => {
        if (!err) res.clearCookie('token');
        res.render('reconnection');
    });
});
app.post('/login', (req, res) => {
    console.log('[ROUTER] ' + req.url);

    request.post({
        url:'http://127.0.0.1:8001/orizon/v1/auth/login',
        form: {
            username: req.body.username,
            password: req.body.password
        }
    }, function(err, httpResponse, body){
        if (err) throw err;

        const bodyJSON = JSON.parse(body);

        if (httpResponse.statusCode == 200) {
            res.cookie('token', bodyJSON.token, {expire : new Date() + 9999});
            request.get({
                url: 'http://127.0.0.1:8001/orizon/v1/auth/myConnections',
                auth: { 'bearer': bodyJSON.token }
            }, function(err, httpResponse, body) {
                if (err) throw err;

                const user = JSON.parse(body);

                if (user.connections.length == 1) {
                    res.redirect('/welcome');
                } else {
                    res.redirect('/');
                }
            });
        } else if (httpResponse.statusCode == 405) {
            res.redirect('/login/bad');
        } else {
            res.redirect('/login');
        }
    });
});

/* 404 error */

app.use(function (req, res) {
    
    res.statusCode = 404;
    res.render('404_page');

});

/* HTTP listening */

app.listen(8000);
