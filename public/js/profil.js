$(document).ready(function(){

    /* Requires */

    const token = getCookie("token");
    const username = document.getElementById('usernameTd').innerText;

    /* Tools */

    function createCORSReq(method, url) {

        let req = new XMLHttpRequest();

        if ("withCredentials" in req) {
            req.open(method, url, true); 
        } else if (typeof XDomainRequest != "undefined") {
            req = new XDomainRequest();
            req.open(method, url, true); 
        } else {
            req.open(method, url, true); 
        }
        return (req);
    }

    /* Cookies */

    function getCookie(cname) {

        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');

        for(var i = 0; i <ca.length; i++) {
            var c = ca[i];

            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    /* Delete account */

    var deleteAccountBtn = document.getElementById('deleteAccountBtn');

    deleteAccountBtn.addEventListener('click', (e) => {
        $("#confirmDeleteModal").modal('show');
    });

    var confirmDeleteBtn = document.getElementById('confirmDeleteBtn');

    confirmDeleteBtn.addEventListener('click', (e) => {
        let req = createCORSReq('DELETE', 'http://127.0.0.1:8001/orizon/v1/auth/user/' + username);

        req.onreadystatechange = function () {
            if(req.readyState === 4 && req.status === 200) {
                window.location = "http://127.0.0.1:8000/my_account/delete";
            }
        };
        req.setRequestHeader("Authorization", "Bearer " + token);
        req.send();
    });

    /* Privileges */

    var privilegesBtn = document.getElementById('privilegesBtn');
    var privilegesSelect = document.getElementById('privilegesSelect');

    privilegesBtn.addEventListener('click', (e) => {
        var privilegesValue = privilegesSelect.options[privilegesSelect.selectedIndex].value;
        let req = createCORSReq('PUT', 'http://127.0.0.1:8001/orizon/v1/auth/privileges/' + username + '/' + privilegesValue);

        req.onreadystatechange = function () {
            if(req.readyState === 4 && req.status === 200) {
                window.location.replace("http://127.0.0.1:8000/my_account");
            }
        };
        req.setRequestHeader("Authorization", "Bearer " + token);
        req.send();
    });
    
});