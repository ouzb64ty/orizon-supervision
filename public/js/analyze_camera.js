$(document).ready(function(){

    /* For ChartJS */

    var canvas = document.getElementById('detectionsStats');
    var token = getCookie("token");

    /* Tools */

    function createCORSReq(method, url) {

        let req = new XMLHttpRequest();

        if ("withCredentials" in req) {
            req.open(method, url, true); 
        } else if (typeof XDomainRequest != "undefined") {
            req = new XDomainRequest();
            req.open(method, url, true); 
        } else {
            req.open(method, url, true); 
        }
        return (req);
    }

    /* Cookies */

    function getCookie(cname) {

        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');

        for(var i = 0; i < ca.length; i++) {
            var c = ca[i];

            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    /* Analyze */

    var analyzeBtn = document.getElementById("analyzeBtn");
    var id = analyzeBtn.parentElement.id;

    analyzeBtn.addEventListener("click", function(e) {
        var request = createCORSReq('POST', 'http://127.0.0.1:8003/vision/v1/active_cameras/');

        request.onreadystatechange = function () {
            if (request.readyState === 4) {
                if (request.status === 200) {
                    var responseJSON = JSON.parse(request.responseText);

                    if (responseJSON.success == 'new') {
                        analyzeBtn.innerHTML = '<span id="analyzeStatIcon" class="fas fa-pause"></span> Pause';
                        $("#successModal").modal('show');
                        $("#analyzeBtn").addClass("btn-outline-primary");
                        $("#analyzeBtn").removeClass("btn-primary");
                    } else if (responseJSON.success == 'pause') {
                        analyzeBtn.innerHTML = '<span id="analyzeStatIcon" class="fas fa-play"></span> Analyser';
                         $("#analyzeBtn").addClass("btn-primary");
                         $("#analyzeBtn").removeClass("btn-outline-primary");
                    } else if (responseJSON.success == 'resume') {
                        analyzeBtn.innerHTML = '<span id="analyzeStatIcon" class="fas fa-pause"></span> Pause';
                        $("#analyzeBtn").addClass("btn-outline-primary");
                        $("#analyzeBtn").removeClass("btn-primary");
                    }
                }
            }
        };
        request.setRequestHeader("Authorization", "Bearer " + token);
        request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        request.send("id=" + id);
        e.preventDefault();
    });

    /* Parse detection API request */

    function getFramesNb(detections) {
        var result = 0;
        var framesNb = [];

        for (var i = 0; i < detections.length; i++) {
            framesNb.push(i);
        }
        return (framesNb);
    }

    function getDate(detections) {
        var tmp = "";
        var time = [];

        for (var i = 0; i < detections.length; i++) {
            tmp = new Date(detections[i].date);
            time.push(tmp.toISOString());
        }
        return (time);
    }

    function getc3Date(detections) {
        var time = [];

        for (var i = 0; i < detections.length; i++) {
            time.push(detections[i].date);
        }
        return (time);
    }

    function getC3LastDate(detections) {
        let time = [];
        time.push(detections.date);
        return (time);
    };

    function getLastFromClass(detections, nbClass) {
        let nb = 0;
        let result = [];
        let tmp = detections.dets;

        
        for (var i = 0; i < tmp.length; i++) {
            if (tmp[i].class == nbClass) nb++;
        }
        result.push(nb);
        return (result);
    };

    function getFromClasse(detections, numClasses) {
        let tmp = 0;
        let result = [];

        for (var i = 0; i < detections.length; i++) {
            for (var y = 0; y < detections[i].dets.length; y++) {
                if (detections[i].dets[y].class == numClasses) tmp++;
            }
            result.push(tmp);
            tmp = 0;
        }
        return (result);
    }

    function getPersonnes(detections) {
        var result = 0;
        var personnes = [];

        for (var i = 0; i < detections.length; i++) {
            for (var y = 0; y < detections[i].dets.length; y++) {
                if (detections[i].dets[y].class == 0) result++;
            }
            personnes.push(result);
            result = 0;
        }
        return (personnes);
    }

    function getLastPersonnes(detections) {
        let result = 0;
        let personnes = [];

        for (var y = 0; y < detections[detections.length - 1].dets.length; y++) {
            if (detections[detections.length - 1].dets[y].class == 0) result++;
        }
        personnes.push(result);
        return (personnes);
    }

    function getVelos(detections) {
        var result = 0;
        var velos = [];

        for (var i = 0; i < detections.length; i++) {
            for (var y = 0; y < detections[i].dets.length; y++) {
                if (detections[i].dets[y].class == 1) result++;
            }
            velos.push(result);
            result = 0;
        }
        return (velos);
    }

    function getVoitures(detections) {
        var result = 0;
        var voitures = [];

        for (var i = 0; i < detections.length; i++) {
            for (var y = 0; y < detections[i].dets.length; y++) {
                if (detections[i].dets[y].class == 2) result++;
            }
            voitures.push(result);
            result = 0;
        }
        return (voitures);
    }

    function getMotos(detections) {
        var result = 0;
        var motos = [];

        for (var i = 0; i < detections.length; i++) {
            for (var y = 0; y < detections[i].dets.length; y++) {
                if (detections[i].dets[y].class == 3) result++;
            }
            motos.push(result);
            result = 0;
        }
        return (motos);
    }

    function getBus(detections) {
        var result = 0;
        var bus = [];

        for (var i = 0; i < detections.length; i++) {
            for (var y = 0; y < detections[i].dets.length; y++) {
                if (detections[i].dets[y].class == 4) result++;
            }
            bus.push(result);
            result = 0;
        }
        return (bus);
    }

    function getCamions(detections) {
        var result = 0;
        var camions = [];

        for (var i = 0; i < detections.length; i++) {
            for (var y = 0; y < detections[i].dets.length; y++) {
                if (detections[i].dets[y].class == 5) result++;
            }
            camions.push(result);
            result = 0;
        }
        return (camions);
    }

    function getSacsAMain(detections) {
        var result = 0;
        var sacsAMain = [];

        for (var i = 0; i < detections.length; i++) {
            for (var y = 0; y < detections[i].dets.length; y++) {
                if (detections[i].dets[y].class == 7) result++;
            }
            sacsAMain.push(result);
            result = 0;
        }
        return (sacsAMain);
    }

    function getSacsADos(detections) {
        var result = 0;
        var sacsADos = [];

        for (var i = 0; i < detections.length; i++) {
            for (var y = 0; y < detections[i].dets.length; y++) {
                if (detections[i].dets[y].class == 6) result++;
            }
            sacsADos.push(result);
            result = 0;
        }
        return (sacsADos);
    }

    function getValises(detections) {
        var result = 0;
        var valises = [];

        for (var i = 0; i < detections.length; i++) {
            for (var y = 0; y < detections[i].dets.length; y++) {
                if (detections[i].dets[y].class == 8) result++;
            }
            valises.push(result);
            result = 0;
        }
        return (valises);
    }

    function getAnimaux(detections) {
        var result = 0;
        var animaux = [];

        for (var i = 0; i < detections.length; i++) {
            for (var y = 0; y < detections[i].dets.length; y++) {
                if (detections[i].dets[y].class == 9) result++;
            }
            animaux.push(result);
            result = 0;
        }
        return (animaux);
    }

    function classIntToString(classes) {
        if (classes == 0) return ('Personne');
        else if (classes == 1) return ('Velo');
        else if (classes == 2) return ('Voiture');
        else if (classes == 3) return ('Moto');
        else if (classes == 4) return ('Bus');
        else if (classes == 5) return ('Camion');
        else if (classes == 6) return ('Sac a dos');
        else if (classes == 7) return ('Sac a main');
        else if (classes == 8) return ('Valise');
        else if (classes == 9) return ('Animal');
        else return ('Unknown');
    }

    function colorIntToString(classes) {
        if (classes == 0) return ('Blanc');
        else if (classes == 1) return ('Noir');
        else if (classes == 2) return ('Rouge');
        else if (classes == 3) return ('Orange');
        else if (classes == 4) return ('Jaune');
        else if (classes == 5) return ('Vert');
        else if (classes == 6) return ('Bleu');
        else if (classes == 7) return ('Violet');
        else if (classes == 8) return ('Magenta');
        else return ('Unknown');
    }

    function dalleDate(seconds) {
        tmp = new Date(seconds);

        return (tmp.toISOString());
    }

    /* Init chart and array detections */

    var request = createCORSReq('GET', 'http://127.0.0.1:8003/vision/v1/detections/cameras/' + id);
    var tbody = document.getElementById('arrayDets');
    var chart;
    var c3Chart;
    const nameClasses = ['Personne', 'Velo', 'Voiture', 'Moto', 'Bus', 'Camion', 'Sac a dos', 'Sac a main', 'Valise', 'Animal'];

    request.onreadystatechange = function () {

        if(request.readyState === 4 && request.status === 200) {
            var responseJSON = JSON.parse(request.response).detections;

            for (var i = 0; i < responseJSON.length; i++) {
                for (var i2 = 0; i2 < responseJSON[i].dets.length; i2++) {
                    var tr = document.createElement('tr');
                    var tdClass = document.createElement('td');
                    var tdColor = document.createElement('td');
                    var tdUrl = document.createElement('td');
                    var tdSec = document.createElement('td');

                    tdSec.innerHTML = '<a target="_blank" href="' + responseJSON[i].src + '">' + dalleDate(responseJSON[i].date) + '</a>';
                    tdClass.innerHTML = classIntToString(responseJSON[i].dets[i2].class);
                    tdColor.innerHTML = colorIntToString(responseJSON[i].dets[i2].color);
                    tr.appendChild(tdSec);
                    tr.appendChild(tdClass);
                    tr.appendChild(tdColor);
                    tbody.appendChild(tr);
                }
            }
            
            /* c3 Stats */
            var c3Data = new Array();

            for (var i = 0; i < nameClasses.length; i++) {
                c3Data[i] = getFromClasse(responseJSON, i);
                c3Data[i].unshift(nameClasses[i]);
            }
            /*var c3Person = getPersonnes(responseJSON);
            var c3Velos = getVelos(responseJSON)
            var c3Animal = getAnimaux(responseJSON);*/
            var c3Date = getc3Date(responseJSON);

            /*c3Person.unshift('personne');
            c3Velos.unshift('velos')
            c3Animal.unshift('Animaux');*/
            c3Date.unshift('x');    

            c3Chart = c3.generate({
                bindto: '#c3Stats',
                data: {
                    columns: [
                        c3Data[0],
                        c3Data[1],
                        c3Data[2],
                        c3Data[3],
                        c3Data[4],
                        c3Data[5],
                        c3Data[6],
                        c3Data[7],
                        c3Data[8],
                        c3Data[9],
                        c3Date
                      ],
                    x: 'x'  
                  ,
                   onclick: function (d, element) {
                           console.log(d);
                   }
                },
                zoom: {
                    enabled: true,
                    onzoom: function (domain) { console.log(domain) }
                },
                subchart: {
                    show: true,
                    onbrush: function (domain) { console.log(domain) }
                },
                axis: {
                    x: {
                        type: 'timeseries',
                        tick: {
                            format: '%H:%M:%S',//format: '%Y-%m-%d %H:%M:%S',
                            max: 24
                        }
                    }
                },
                size: {
                    height: "600"
                }
            });

        }
    };
    request.setRequestHeader("Authorization", "Bearer " + token);
    request.send();

    /* WS add detections */

    let wsVision = io.connect('http://127.0.0.1:8002');
    //var progressBar = document.getElementById("progressBar");

    wsVision.on('dets', function(data) {
        if (data.assoc == id) {
            for (var i2 = 0; i2 < data.dets.length; i2++) {
                var tr = document.createElement('tr');
                var tdClass = document.createElement('td');
                var tdColor = document.createElement('td');
                var tdUrl = document.createElement('td');
                var tdSec = document.createElement('td');

                tdSec.innerHTML = '<a target="_blank" href="' + data.src + '">' + dalleDate(data.progressSec) + '</a>';
                tdClass.innerHTML = classIntToString(data.dets[i2].class);
                tdColor.innerHTML = colorIntToString(data.dets[i2].color);
                tr.appendChild(tdSec);
                tr.appendChild(tdClass);
                tr.appendChild(tdColor);
                tbody.appendChild(tr);
            }
            
            /* c3 update */
            var c3LastData = new Array();
            var c3LastDate = getC3LastDate(data);
            c3LastDate.unshift('x');

            for (var i = 0; i < nameClasses.length; i++) {
                c3LastData[i] = getLastFromClass(data, i);//getLastFromClass(responseJSON, i);
                c3LastData[i].unshift(nameClasses[i]);
            }
            c3Chart.flow({
                columns: [
                    c3LastData[0],
                    c3LastData[1],
                    c3LastData[2],
                    c3LastData[3],
                    c3LastData[4],
                    c3LastData[5],
                    c3LastData[6],
                    c3LastData[7],
                    c3LastData[8],
                    c3LastData[9],
                    c3LastDate
                ]
            });
        }
    });

    /* Edit camera Modal */

    var editCameraBtn = document.getElementById("editCameraBtn");

    editCameraBtn.addEventListener("click", function(e) {
        $("#editModal").modal('show');
        e.preventDefault();
    });

    /* Edit domain */

    var editDomainBtn = document.getElementById("editDomainBtn");

    editDomainBtn.addEventListener("click", function(e) {
        var editDomainInput = document.getElementById("editDomainInput").value;
        var request = createCORSReq('PUT', 'http://127.0.0.1:8001/orizon/v1/cameras/domain/' + id);

        request.onreadystatechange = function () {
            if(request.readyState === 4 && request.status === 200) {
                location.reload(true);
            }
        };
        request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        request.setRequestHeader("Authorization", "Bearer " + token);
        request.send("domain=" + editDomainInput);
    });

    /* Edit description */

    var editDescriptionBtn = document.getElementById("editDescriptionBtn");

    editDescriptionBtn.addEventListener("click", function(e) {
        var editDescriptionInput = document.getElementById("editDescriptionInput").value;
        var request = createCORSReq('PUT', 'http://127.0.0.1:8001/orizon/v1/cameras/description/' + id );

        request.onreadystatechange = function () {
            if(request.readyState === 4 && request.status === 200) {
                location.reload(true);
            }
        };
        request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        request.setRequestHeader("Authorization", "Bearer " + token);
        request.send("description=" + editDescriptionInput);
    });

    /* Remove camera Modal */

    var removeCameraBtn = document.getElementById("removeCameraBtn");

    removeCameraBtn.addEventListener("click", function(e) {
        $("#removeModal").modal('show');
        e.preventDefault();

        var trashCamera = document.getElementById("trash");

        trashCamera.addEventListener("click", function(e) {
            let req = createCORSReq('DELETE', 'http://127.0.0.1:8001/orizon/v1/cameras/' + id);

            req.onreadystatechange = function () {
                if(req.readyState === 4 && req.status === 200) {
                    document.location.href = 'http://127.0.0.1:8000/list_camera';
                }
            };
            req.setRequestHeader("Authorization", "Bearer " + token);
            req.send();
        });
        e.preventDefault();
    });

    /* Refresh camera snapshot */

    var img = document.getElementById('IRTCam');
    var tmpSrc = img.src;

    if (tmpSrc.indexOf('mjpg') == -1 && tmpSrc.indexOf('mjpeg') == -1) {
        setInterval(function() {
            img.src = tmpSrc + '?random=' + new Date().getTime();
        }, 1000);
    }

});