/* Tools */

function createCORSReq(method, url) {

    let req = new XMLHttpRequest();

    if ("withCredentials" in req) {
        req.open(method, url, true); 
    } else if (typeof XDomainRequest != "undefined") {
        req = new XDomainRequest();
        req.open(method, url, true); 
    } else {
        req.open(method, url, true); 
    }
    return (req);
}

/* Cookies */

function getCookie(cname) {

    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

/* Trash buttons */

const trashCameras = document.getElementsByName("trash");
const trashCamerasList = Array.prototype.slice.call(trashCameras);
const token = getCookie("token");

trashCamerasList.forEach(function(camera, index, list) {
    var modal_card = camera.parentElement.parentElement;

    camera.addEventListener("click", function(e) {
        $("#removeModal").modal('show');
        e.preventDefault();

        var confTrash = document.getElementById("confTrash");

        confTrash.addEventListener("click", function(e) {
            let req = createCORSReq('DELETE', 'http://127.0.0.1:8001/orizon/v1/cameras/' + modal_card.id);

            req.onreadystatechange = function () {
                if(req.readyState === 4 && req.status === 200) {
                    document.location.reload(true);
                }
            };
            req.setRequestHeader("Authorization", "Bearer " + token);
            req.send();
        });
    });
});


/* Analyze btn */

$(document).ready(function() {
    let request = createCORSReq('GET', 'http://127.0.0.1:8003/vision/v1/active_cameras/');
    var activate = [];
    request.onreadystatechange = function () {
        if (request.readyState === 4) {
            if (request.status === 200) {
                let responseJSON = JSON.parse(request.responseText);
                responseJSON.active_cameras.forEach((item) => {
                    if (item.jsonRes.isActive === true)
                        activate.push(item.jsonRes._id);
                });
                activate.forEach((item) => {
                    $("#btn" + item).empty();
                    $("#btn" + item).append('<span id="analyzeStatIcon" class="fas fa-pause"></span> Pause');
                    $("#btn" + item).addClass("btn-outline-primary");
                    $("#btn" + item).removeClass("btn-primary");
                });
            };

        };
    };
    request.setRequestHeader("Authorization", "Bearer " + token);
    request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    request.send();
});

const analyzeCameras = document.getElementsByName("listAnalyzeBtn");
const analyzeCamerasList = Array.prototype.slice.call(analyzeCameras);

analyzeCamerasList.forEach(function(camera, index, list) {
    let id = camera.parentElement.parentElement.id;

    camera.addEventListener("click", function(e) {
        var request = createCORSReq('POST', 'http://127.0.0.1:8003/vision/v1/active_cameras/');
        let cam = "#btn" + id;

        request.onreadystatechange = function () {
            if (request.readyState === 4) {
                if (request.status === 200) {
                    let responseJSON = JSON.parse(request.responseText);
                    if (responseJSON.success == 'new') {
                        $("#successModal").modal('show');
                        $(cam).empty();
                        $(cam).append('<span id="analyzeStatIcon" class="fas fa-pause"></span> Pause');
                        $(cam).addClass("btn-outline-primary");
                        $(cam).removeClass("btn-primary");
                    } else if (responseJSON.success == 'pause') {
                        $(cam).empty();
                        $(cam).append('<span id="analyzeStatIcon" class="fas fa-play"></span> Analyser');
                        $(cam).addClass("btn-primary");
                        $(cam).removeClass("btn-outline-primary");
                    } else if (responseJSON.success == 'resume') {
                        $(cam).empty();
                        $(cam).append('<span id="analyzeStatIcon" class="fas fa-pause"></span> Pause');
                        $(cam).addClass("btn-outline-primary");
                        $(cam).removeClass("btn-primary");
                    }
                }
            }
        };
        request.setRequestHeader("Authorization", "Bearer " + token);
        request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        request.send("id=" + id);
        e.preventDefault();
    });
});