const Stream = require('node-rtsp-stream');
const processMod = require('process');

var stream = new Stream({
    name: processMod.argv[2],
    streamUrl: processMod.argv[3],
    wsPort: processMod.argv[4],
    ffmpegOptions: {
        '-stats': '',
        '-r': 30
    }
});