var dbs = db.getMongo().getDBNames();

for (var i in dbs) {
    db = db.getMongo().getDB( dbs[i] );
    
    print("[DROP] dropping db " + db.getName());
    db.dropDatabase();
}