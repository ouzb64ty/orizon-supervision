module.exports = class Auth {

    /**
     * @api {post} /orizon/v1/auth/register Register a new user
     * @apiName register
     * @apiGroup auth
     *
     * @apiSuccess {Object} success User now registered
     *
     * @apiParam {String} username Username.
     * @apiParam {String} password Password.
     * @apiParam {String} mail Mail.
     * @apiParam {String} privileges boolean true or false.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          'success': 'User now registered'
     *     }
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 400 Bad Request
     *     {
     *          'error': "Bad parameters"
     *     }
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *          'error': "User already exists"
     *     }
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *     {
     *          'error': 'Internal Server Error'
     *     }
     */

    static register(db, req, res, user)
    {
        const bcrypt = require('bcrypt');
        const datetime = require('node-datetime');
        const Isemail = require('isemail');

        /*if (user.payload.is_admin == true) {}*/
        if (req.body.password != req.body.conf_password) {
            res.statusCode = 400;
            res.json({
                "error": "Bad parameters"
            });
            return ;
        }

        /* Good params */
        if (req.body.username.length < 4 || req.body.password.length < 8) {
            res.statusCode = 400;
            res.json({
                "error": "Bad parameters"
            });
            return ;
        }

        /* Empty verification */
        if (req.body.username == null || req.body.password == null || req.body.mail == null) {
            res.statusCode = 400;
            res.json({
                "error": "Empty parameters"
            });
            return ;
        }

        /* Email verification */
        if (!Isemail.validate(req.body.mail)) {
            res.statusCode = 400;
            res.json({
                "error": "Bad email"
            });
            return ;
        }

        db.collection('users').findOne({ 'username' : req.body.username },
            (err, user) => {

                if (err) {

                    res.statusCode = 500;
                    res.json({
                        "error": "Internal Server Error"
                    });
                    throw err;

                } else {

                    if (user) {

                        /* User already exist */
                        res.statusCode = 404;
                        res.json({
                            "error": "User already exists"
                        });

                    } else {
                        /* Register user */
                        bcrypt.hash(req.body.password, 10, (err, hash) => {

                            if (err) {

                                res.statusCode = 500;
                                res.json({
                                    "error": "Internal Server Error"
                                });
                                throw err;

                            } else {

                                const dt = datetime.create();
                                const formatted = dt.format('m/d/Y H:M:S');
                                const is_admin = (req.body.is_admin == "true");

                                db.collection('users').insertOne({
                                    'username': req.body.username,
                                    'password': hash,
                                    'mail': req.body.mail,
                                    'is_admin': is_admin,
                                    'datetime': formatted
                                }, (err, dbResult) => {
                                    if (err) {

                                        res.statusCode = 500;
                                        res.json({
                                            "error": "Internal Server Error"
                                        });
                                        throw err;

                                    } else {

                                        res.json({
                                            "success": "User now registered"
                                        });

                                    }
                                });
                            }
                        });
                    }
                }
            });
    }

    /**
     * @api {post} /orizon/v1/auth/login Obtain Bearer Token to use Orizon collections
     * @apiName login
     * @apiGroup auth
     *
     * @apiSuccess {Object} token Bearer Token to use
     *
     * @apiParam {String} username Username.
     * @apiParam {String} password Password.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          'token': 'ZwCdVf187451'
     *     }
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 405 Method Not Allowed
     *     {
     *          'error': "User doesn\'t exists"
     *     }
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *     {
     *          'error': 'Internal Server Error'
     *     }
     */

    static login(db, req, res, user_) {

        const bcrypt = require('bcrypt');
        const jwt = require('jsonwebtoken');
        const http = require('http');
        const datetime = require('node-datetime');
        const dt = datetime.create();
        const formatted = dt.format('le m/d/Y à H:M:S');

        /* Find user by username */
        db.collection('users').findOne({ 'username' : req.body.username },
            (err, user) => {
                if (err) {

                    res.statusCode = 500;
                    res.json({
                         "error": "Internal Server Error"
                    });
                    throw err;

                } else {

                    if (user) {
                        /* Compare passwords */
                        bcrypt.compare(req.body.password, user.password, (err, resCmp) => {
                            if (resCmp == true) {

                                const hpass = req.body.password.replace(/./g, '•');
                                /* Generate JWT */
                                const payload = {
                                    'username': user.username,
                                    'mail': user.mail,
                                    'datetime': user.datetime,
                                    'is_admin': user.is_admin,
                                    'hpass': hpass
                                };
                                const token = jwt.sign({payload}, 'jwtRS256.key', { expiresIn: 604800 } );

                                /* Update connections */
                                db.collection('connections').insertOne({
                                    'username': user.username,
                                    'datetime': formatted
                                }, (err, dbResult) => { if (err) throw err; });
                                /* Send token */
                                res.json({
                                    'token': token
                                });

                            } else {

                                res.statusCode = 405;
                                res.json({
                                    'error': 'User doesn\'t exists'
                                });
                            }
                        });
                    } else {

                        res.statusCode = 405;
                        res.json({
                            'error': 'User doesn\'t exists'
                        });

                    }
                }
            });
    }

    /**
     * @api {get} /orizon/v1/auth/myConnections Get all my connections (Need admin privileges)
     * @apiName getMyConnections
     * @apiGroup auth
     *
     * @apiHeader {String} token Bearer Token authorization.
     *
     * @apiSuccess {Object} connections All connections
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          'connections': [{
     *              "_id" : ObjectId("5d1615b057b1666f00a3c49a"), 
     *              "username" : "admin", 
     *              "datetime" : "06/28/2019 15:27:12"
     *          }]
     *     }
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *     {
     *          'error': 'Internal Server Error'
     *     }
     */

    static getMyConnections(db, req, res, user) {
        db.collection('connections').find({'username': user.payload.username}).toArray(
            (err, connections) => {
                if (err) {

                    res.statusCode = 500;
                    res.json({
                        'error': 'Internal Server Error'
                    });
                    throw err;

                } else {

                    res.json({connections});

                }

            });
    }

    /**
     * @api {get} /orizon/v1/connections Get all connections (Need admin privileges)
     * @apiName getConnections
     * @apiGroup auth
     *
     * @apiHeader {String} token Bearer Token authorization.
     *
     * @apiSuccess {Object} connections All connections
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          'connections': [{
     *              "_id" : ObjectId("5d1615b057b1666f00a3c49a"), 
     *              "username" : "admin", 
     *              "datetime" : "06/28/2019 15:27:12"
     *          }]
     *     }
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 401 Unauthorized
     *     {
     *          'error': 'This request need administrator privileges'
     *     }
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *     {
     *          'error': 'Internal Server Error'
     *     }
     */

    static getConnections(db, req, res, user) {
        if (user.payload.is_admin == true) {

            db.collection('connections').find({}).toArray(
                (err, connections) => {
                    if (err) {

                        res.statusCode = 500;
                        res.json({
                            'error': 'Internal Server Error'
                        });
                        throw err;

                    } else {

                        res.json({connections});

                    }

                });

        } else {
            res.statusCode = 401;
            res.json({
                'error': 'Need administrator privileges'
            });
        }
    }

    /**
     * @api {get} /orizon/v1/auth/users/:username Get user (Need admin privileges)
     * @apiName getUser
     * @apiGroup auth
     *
     * @apiHeader {String} token Bearer Token authorization.
     *
     * @apiSuccess {Object} connections All connections
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          'user': {}
     *     }
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *     {
     *          'error': 'Internal Server Error'
     *     }
     */

    static getUser(db, req, res, user) {
        if (user.payload.is_admin == true) {

            db.collection('users').findOne({'username': req.params.username}, (err, user) => {
                    if (err) {

                        res.statusCode = 500;
                        res.json({
                            'error': 'Internal Server Error'
                        });
                        throw err;

                    } else {

                        res.json(user);

                    }

                });

        } else {
            res.statusCode = 401;
            res.json({
                'error': 'Need administrator privileges'
            });
        }
    }

    /**
     * @api {put} /orizon/v1/auth/users/ Get all users (Need admin privileges)
     * @apiName getUsers
     * @apiGroup auth
     *
     * @apiHeader {String} token Bearer Token authorization.
     *
     * @apiSuccess {Object} connections All connections
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          'user': {}
     *     }
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *     {
     *          'error': 'Internal Server Error'
     *     }
     */

    static getUsers(db, req, res, user) {
        if (user.payload.is_admin == true) {

            db.collection('users').find({}).toArray((err, users) => {
                if (err) {

                    res.statusCode = 500;
                    res.json({
                        'error': 'Internal Server Error'
                    });
                    throw err;

                } else {

                    res.json({users});

                }

            });

        } else {
            res.statusCode = 401;
            res.json({
                'error': 'Need administrator privileges'
            });
        }
    }

    /**
     * @api {put} /orizon/v1/auth/username/{username} Modify self username
     * @apiName putUsername
     * @apiGroup auth
     *
     * @apiHeader {String} token Bearer Token authorization.
     *
     * @apiSuccess {Object} connections All connections
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          'success': 'Username correctly update'
     *     }
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 400 Bad Request
     *     {
     *          'error': 'Wrong username'
     *     }
     */

    static putUsername(db, req, res, user) {
        if (req.params.username.length < 4) {
            res.statusCode = 400;
            res.json({
                "error": "Wrong username"
            });
        } else {
            db.collection("users").updateOne({"username": user.payload.username}, {$set: {"username": req.params.username}});
            db.collection("connections").updateOne({"username": user.payload.username}, {$set: {"username": req.params.username}});
            res.statusCode = 200;
            res.json({
                'success': 'Username correctly update'
            });
        }
    }

    /**
     * @api {put} /orizon/v1/auth/mail/{mail} Modify self mail
     * @apiName putMail
     * @apiGroup auth
     *
     * @apiHeader {String} token Bearer Token authorization.
     *
     * @apiSuccess {Object} connections All connections
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          'success': 'Mail correctly update'
     *     }
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 400 Bad Request
     *     {
     *          'error': 'Wrong mail'
     *     }
     */

    static putMail(db, req, res, user) {
        const Isemail = require('isemail');

        /* Email verification */
        if (!Isemail.validate(req.params.mail)) {
            res.statusCode = 400;
            res.json({
                "error": "Wrong mail"
            });
        } else {
            db.collection("users").updateOne({"username": user.payload.username}, {$set: {"mail": req.params.mail}});
            res.statusCode = 200;
            res.json({
                'success': 'Mail correctly update'
            });
        }
    }

    /**
     * @api {put} /orizon/v1/auth/password/{password} Modify self password
     * @apiName putPassword
     * @apiGroup auth
     *
     * @apiHeader {String} token Bearer Token authorization.
     *
     * @apiSuccess {Object} connections All connections
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          'success': 'Password correctly update'
     *     }
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 401 Unauthorized
     *     {
     *          'error': 'Wrong password'
     *     }
     */

    static putPassword(db, req, res, user) {
        const bcrypt = require('bcrypt');

        db.collection('users').findOne({'username': user.payload.username}, (err, ext_user) => {
            if (err) {

                res.statusCode = 500;
                res.json({
                    'error': 'Internal Server Error'
                });
                throw err;

            } else {

                /* Compare passwords */
                bcrypt.compare(req.body.lastPassword, ext_user.password, (err, resCmp) => {
                    if (resCmp == true) {
                        bcrypt.hash(req.params.password, 10, (err, hash) => {
                            if (err) {
                                res.statusCode = 500;
                                res.json({
                                    'error': 'Internal Server Error'
                                });
                                throw err;
                            }
                            db.collection("users").updateOne({"username": user.payload.username}, {$set: {"password": hash}});
                            res.statusCode = 200;
                            res.json({
                                'success': 'Password correctly update'
                            });
                        });
                    } else {
                        res.statusCode = 401;
                        res.json({
                            'error': 'Wrong password'
                        });
                    }
                });

            }

         });

    }

    /**
     * @api {put} /orizon/v1/auth/password/{password} Modify user privileges
     * @apiName putPrivileges
     * @apiGroup auth
     *
     * @apiHeader {String} token Bearer Token authorization.
     *
     * @apiSuccess {Object} connections All connections
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          'success': 'User privileges correctly update'
     *     }
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 400 Bad Request
     *     {
     *          'error': 'Wrong privilege value (0 or 1)'
     *     }
     *
     */

    static putPrivileges(db, req, res, user) {
        let value = parseInt(req.params.bool);

        if (value == 1) value = true;
        else if (value == 0) value = false;
        else {
            res.statusCode = 400;
            res.json({
                'error': 'Wrong privilege value (0 or 1)'
            });
        }

        db.collection("users").updateOne({"username": req.params.username}, {$set: {"is_admin": value}});
        res.statusCode = 200;
        res.json({
            'success': 'User privilege correctly update'
        });
    }

    /**
     * @api {delete} /orizon/v1/auth/user/{username} Delete user by username
     * @apiName deleteUser
     * @apiGroup auth
     *
     * @apiHeader {String} token Bearer Token authorization.
     *
     * @apiSuccess {Object} connections All connections
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          'success': 'User correctly removed with assiocated datas'
     *     }
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 401 Unauthorized
     *     {
     *          'error': 'Need administrator privileges'
     *     }
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 500 Internal Server Error
     *     {
     *          'error': 'Internal Server Error'
     *     }
     */

    static deleteUser(db, req, res, user) {
        if (user.payload.is_admin == true || user.payload.username == req.params.username) {
            db.collection('users').deleteOne(
                { 'username': req.params.username },
                (err, dbResult) => {
                    if (err) {
                        res.statusCode = 500;
                        res.json({
                            'error': 'Internal Server Error'
                        });
                    } else {
                        res.statusCode = 200;
                        res.json({
                            'success': 'User correctly removed'
                        });
                    }
                });
        } else {
            res.statusCode = 401;
            res.json({
                'error': 'Need administrator privileges'
            });
        }
    }

};